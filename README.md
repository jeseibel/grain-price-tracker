# Grain Price Tracker

This program polls the website https://arthurcompanies.com/locations/harvey-pairie-towers/ at given intervals and writes the results to a file so grain prices can be tracked over time.


Run command: `./gradlew run` \

Build command: `./gradlew build`  
the compiled jar will be placed in: `build/libs/YOUR_JAR_NAME-all.jar`

Run compiled jar: `java -jar ./YOUR_JAR_NAME-all.jar`
