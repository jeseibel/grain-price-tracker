package test;

import com.seibel.controllers.EmailApi;
import com.seibel.controllers.WebReader;
import org.junit.Assert;
import org.junit.Test;

import java.util.Properties;
import java.util.regex.Pattern;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class TestClass
{
	
	@Test
	public void sendTestEmail()
	{
		EmailApi api = new EmailApi(
				"jeseibel@gondtc.com",
				"jeseibel@gondtc.com",
				"jeseibel",
				"MySuperCoolEmailPassword"
		);
		
		
		try
		{
			api.sendEmail("Java Test", "test message");
			System.out.println("message sent");
		}
		catch (Exception e)
		{
			String message = "message not sent. Error: [" + e.getMessage() + "]";
			System.out.println(message);
			Assert.fail(message);
		}
		
	}
	
	@Test
	public void conversionTest()
	{
		String str = " 854'2s";
		Assert.assertEquals("8.5425", WebReader.convertFutureToDollarString(str));
		
		str = " 55'4s";
		Assert.assertEquals("0.555", WebReader.convertFutureToDollarString(str));
		
		str = " 5'4s";
		Assert.assertEquals("0.055", WebReader.convertFutureToDollarString(str));
		
	}
	
}