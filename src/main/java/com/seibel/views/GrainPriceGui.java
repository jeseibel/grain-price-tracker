package com.seibel.views;

import java.io.File;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import com.seibel.controllers.ConfigHandler;
import com.seibel.controllers.WebReader;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;

/** is the application that should be called to start the program */
public class GrainPriceGui extends Application implements EventHandler<ActionEvent>
{
	public final static int WINDOW_HEIGHT = 64;
	public final static int WINDOW_WIDTH = 64*4 + 48;
	public static final String DEFAULT_FILE_EXTENSION = ".tsv";
	
	/** this button starts or stops the model */
	private Button timerControlButton;
	private Button pollButton;
	
	/** this button shows the folder chooser */
	private Button folderButton;
	
	/** This displays the current status of the program (whether the model is currently running or not) */
	private TextArea statusDisplay;
	
	/** This is the reference to the WebReader model */
	private WebReader webReader;
	
	/** This holds whether the model is running or not */
	private boolean webReaderRunning = false;
	
	/** this is the reference to the model thread */
	public Thread modelThread;
	
	/** this is the reference to the GUI window */
	private Stage gui;
	
	/** this is the file where all output is written to */
	private File exportDir;
	
	/** this timer is used to determine when next to poll the website */
	private Timer timer;
	
	/** this is the date when the website will next be polled */
	private Calendar nextDate;
	
	
	
	//=============//
	// constructor //
	//=============//
	
	@Override
	public void start(Stage primaryStage) 
	{
		// create the start/stop button
		this.timerControlButton = new Button();
		this.timerControlButton.setText("Start");
		this.timerControlButton.setOnAction(this);
		
		// create the web poll button
		this.pollButton = new Button();
		this.pollButton.setText("Poll Now");
		this.pollButton.setOnAction(this);
		
		// create the folder button
		this.folderButton = new Button();
		this.folderButton.setText("Change save folder");
		this.folderButton.setOnAction(this);
		
		// create the status display TextField
		this.statusDisplay = new TextArea();
		this.statusDisplay.setText("Ready");
		this.statusDisplay.setWrapText(true);
		this.statusDisplay.setEditable(false);
		this.statusDisplay.minHeight(128);
		this.statusDisplay.maxHeight(128);
		
		
		// create the internal GUI
		BorderPane root = new BorderPane();
		
		// add buttons
		root.setLeft(this.timerControlButton);
		BorderPane.setAlignment(this.timerControlButton, Pos.TOP_CENTER);
		root.setCenter(this.pollButton);
		BorderPane.setAlignment(this.pollButton, Pos.TOP_CENTER);
		root.setRight(this.folderButton);
		BorderPane.setAlignment(this.folderButton, Pos.TOP_CENTER);
		
		// add the status display
		root.setBottom(this.statusDisplay);
		
		// set what happens when the program is closed
		primaryStage.setOnCloseRequest((windowEvent) -> this.quit());
		
		// create the scene
		Scene scene = new Scene(root,WINDOW_WIDTH,WINDOW_HEIGHT);
		scene.getStylesheets().add("application.css"); // path relative to the resources folder
		
		// set the icon
		primaryStage.getIcons().add(new Image("logo.png")); // path relative to the resources folder
		
		// set what is to be displayed
		primaryStage.setScene(scene);
		primaryStage.show();
		
		// title the window
		primaryStage.setTitle("Grain Price Tracker");
		
		// make it so that the gui will go on top of the browser
//		primaryStage.setAlwaysOnTop(true);
		
		// this is so the GUI can be referenced outside this method
		this.gui = primaryStage;
		
		
		
		this.getDirectoryAndStartWebReader();
	}

	/**
	 * Check if the config file has a save folder it,
	 * if it does use that folder and start the program,
	 * if it doesn't ask the user for a directory and then start the program.
	 */
	private void getDirectoryAndStartWebReader()
	{
		// create the info alert
		Alert selectExportFileAlert = new Alert(AlertType.INFORMATION);
		selectExportFileAlert.initOwner(this.gui);
		selectExportFileAlert.setHeaderText("");
		selectExportFileAlert.setTitle("Info");
		selectExportFileAlert.setContentText("In the next window, please select where you would like to create the export files.\n"
				+ "If export files already exist, please select the folder where they are located.");
		
		
		String previousExportFileLocation = ConfigHandler.INSTANCE.getValue(ConfigHandler.EXPORT_FILE_LOCATION_KEY);
		if (previousExportFileLocation != null)
		{
			this.exportDir = new File(previousExportFileLocation);
		}
		
		// if the config file doesn't exist or there is no initial Directory
		// ask the user to pick one
		while(this.exportDir == null)
		{
			// show the info dialogue
			if (selectExportFileAlert.showAndWait().isEmpty())
			{
				// the user clicked the X button, not the OK button
				this.quit();
			}
			// clear the previous button result
			selectExportFileAlert.setResult(null);
			
			this.showDirectoryPicker(null);
		}
		
		
		
		//============================//
		// create and start the model //
		//============================//
		
		
		// create a new web interface
		try
		{
			Alert loadingAlert = new Alert(AlertType.INFORMATION);
			loadingAlert.initOwner(this.gui);
			loadingAlert.setHeaderText("");
			loadingAlert.setTitle("Loading...");
			loadingAlert.setContentText("Loading...");
			
			loadingAlert.show();
			
			// create the model
			this.webReader = new WebReader(this.exportDir, '\t', DEFAULT_FILE_EXTENSION);
			loadingAlert.close();
			
			
			// start the model
			this.startWebReader();
		}
		catch (Exception e)
		{
			Alert errorAlert = new Alert(AlertType.ERROR);
			errorAlert.initOwner(this.gui);
			errorAlert.setHeaderText("Unexpected setup error");
			errorAlert.setTitle("Error");
			errorAlert.setContentText(e.getMessage());
			// stop the program after closing
			errorAlert.setOnCloseRequest((dialogEvent) -> this.gui.close());
			
			errorAlert.show();
		}
	}

	/** Ask the user for a new save folder location. */
	private void showDirectoryPicker(File initialDirectory)
	{
		// create the directory chooser, that starts at the desktop
		DirectoryChooser dirChooser = new DirectoryChooser();
		
		// if we don't have the previously opened folder, default to the user's desktop
		if(initialDirectory == null)
		{
			initialDirectory = new File(System.getProperty("user.home") + "/Desktop");
		}
		dirChooser.setInitialDirectory(initialDirectory);
		
		
		// ask the user where they would like to save too
		this.exportDir = dirChooser.showDialog(this.gui);
				
		
		if (this.exportDir != null)
		{
			ConfigHandler.INSTANCE.setValue(ConfigHandler.EXPORT_FILE_LOCATION_KEY, this.exportDir.getAbsolutePath());
			System.out.println("New save location: \"" + this.exportDir + "\"");
		}
	}
	
	/** this stops the model if it is running. */
	private void stopModel()
	{
		// only continue if the program is already running
		if(this.webReaderRunning)
		{
			this.webReaderRunning = false;
			
			this.timerControlButton.setText("Resume");
			this.statusDisplay.setText("PAUSED");
			
			this.timer.cancel();
		}
	}
	
	private void startWebReader()
	{
		// only continue if the program isn't already running
		if(!this.webReaderRunning)
		{
			// start the program
			this.webReaderRunning = true;
			
			this.timerControlButton.setText("Pause");
			
			// get the day we want to run this again
			this.nextDate = Calendar.getInstance();
			this.nextDate.set(Calendar.HOUR_OF_DAY, 14); // 2 pm
			this.nextDate.set(Calendar.MINUTE, 0);
			this.nextDate.set(Calendar.SECOND, 0);
			
			// create the timer
			this.timer = new Timer();
			this.timer.schedule(new TimerTask()
			{
				@Override
				public void run()
				{
					nextDate.add(Calendar.DAY_OF_MONTH, 1);
					
					// has to be run on javafx thread otherwise an index out of bounds exception could happen
					// source: https://stackoverflow.com/questions/30863862/javafx-append-text-to-textarea-throws-exception
					javafx.application.Platform.runLater( () -> statusDisplay.setText("RUNNING - Website will be polled on: " + calendarToString(nextDate)) );
					
					readWebAsync();
				}
			},
					this.nextDate.getTime(), // this is the time the timer will start
			TimeUnit.MILLISECONDS.convert(1, TimeUnit.DAYS)); // period: 1 day
			
			// show when the program will next run
			this.statusDisplay.setText("RUNNING - Website will be polled at: " + this.calendarToString(this.nextDate));
		}
	}
	
	/** This creates a new thread that runs the WebInterface. */
	private void readWebAsync()
	{
		// only try to run the interface if one isn't already running
		if(this.modelThread == null || !this.modelThread.isAlive())
		{
			// start a new thread
			this.modelThread = new Thread(this.webReader);
			
			// start the thread
			this.modelThread.start();
		}
	}
	
	private void quit()
	{
		System.out.println("Shutting down program...");
		
		if (this.timer != null)
		{
			this.timer.cancel();
		}
		
		if (this.webReader != null)
		{
			this.webReader.quit();
		}
		
		System.out.println("Shutdown complete.");
		
		// stop the java virtual machine
		System.exit(0);
	}
	
	/** This method turns a date object into a human-readable string */
	private String calendarToString(Calendar date)
	{
		if (date == null)
		{
			return "null";
		}
		
		// hours
		String str = date.get(Calendar.HOUR) + ":";
		
		// minutes
		if(date.get(Calendar.MINUTE) < 9)
		{
			str += "0" + date.get(Calendar.MINUTE);
		}
		else
		{
			str += date.get(Calendar.MINUTE);
		}
		
		str += " "; // blank space for readability
		
		// AM / PM
		str += ((date.get(Calendar.AM_PM) == Calendar.AM)? "AM ":"PM ");
		
		// Date
		str += (date.get(Calendar.MONTH) + 1) + "/" + date.get(Calendar.DAY_OF_MONTH) + "/" + date.get(Calendar.YEAR);
		
		return str;
	}
	
	@Override
	public void handle(ActionEvent event) 
	{
		if(event.getSource() == this.timerControlButton)
		{
			if(!this.webReaderRunning)
			{
				this.startWebReader();
			}
			else
			{
				this.stopModel();
			}
		}
		else if (event.getSource() == this.pollButton)
		{
			Alert pollingAlert = new Alert(AlertType.INFORMATION);
			pollingAlert.initOwner(this.gui);
			pollingAlert.setHeaderText("");
			pollingAlert.setTitle("Polling...");
			pollingAlert.setContentText("Attempting to poll website now.");
			pollingAlert.show();
			
			this.readWebAsync();
		}
		else if(event.getSource() == this.folderButton)
		{
			this.showDirectoryPicker(this.exportDir);
		}
	}// end of handle method
	
}
