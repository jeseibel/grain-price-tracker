package com.seibel.launcher;

import com.seibel.views.GrainPriceGui;
import javafx.application.Application;

/**
 * a separate launcher class is necessary due to how modules work when packaged into a single jar
 * see: 
 * https://stackoverflow.com/questions/70175403/how-to-generate-javafx-jar-from-gradle-including-all-dependencies
 * for more information
 */
public class Main
{
	public static void main(String[] args)
	{
		Application.launch(GrainPriceGui.class, args);
	}
}
