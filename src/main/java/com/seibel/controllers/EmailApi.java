package com.seibel.controllers;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * @author James_Seibel
 * @version 2-12-2022
 */
public class EmailApi
{
	public static final EmailApi INSTANCE = new EmailApi();
	
	public final String recipientEmail;// = "jeseibel@gondtc.com";
	public final String senderEmail;// = "jeseibel@gondtc.com";
	public final String senderUsername;// = "jeseibel";
	public final String senderPassword;// = "MySuperCoolPasswordThatYouWouldNeverEverGuess1";
	
	private final Properties properties = System.getProperties();
	
	
	//==============//
	// constructors //
	//==============//
	
	private EmailApi()
	{
		this.recipientEmail = ConfigHandler.INSTANCE.getValue(ConfigHandler.RECIPIENT_EMAIL_KEY);
		this.senderEmail = ConfigHandler.INSTANCE.getValue(ConfigHandler.SENDER_EMAIL_KEY);
		this.senderUsername = ConfigHandler.INSTANCE.getValue(ConfigHandler.SENDER_EMAIL_USERNAME_KEY);
		this.senderPassword = ConfigHandler.INSTANCE.getValue(ConfigHandler.SENDER_EMAIL_PASSWORD_KEY);
		
		if (this.recipientEmail == null || this.senderEmail == null 
			|| this.senderUsername == null || this.senderPassword == null)
		{
			throw new RuntimeException("Missing or incomplete authentication configuration");
		}
		
		
		this.properties.setProperty("mail.smtp.host", "mail.gondtc.com");
		this.properties.setProperty("mail.smtp.ssl.enable", "true");
		this.properties.setProperty("mail.smtp.port", "465");
		this.properties.setProperty("mail.smtp.auth", "true");
		this.properties.setProperty("mail.smtp.ssl.protocols", "TLSv1.2"); // this is required for authentication on some machines (specifically windows 11)
	}
	
	public EmailApi(String recipientEmail, String senderEmail, String senderUsername, String senderPassword)
	{
		this.recipientEmail = recipientEmail;
		this.senderEmail = senderEmail;
		this.senderUsername = senderUsername;
		this.senderPassword = senderPassword;
		
		if (recipientEmail == null || senderEmail == null || senderUsername == null || senderPassword == null)
		{
			throw new RuntimeException("Missing or incomplete authentication configuration");
		}
		
		
		this.properties.setProperty("mail.smtp.host", "mail.gondtc.com");
		this.properties.setProperty("mail.smtp.ssl.enable", "true");
		this.properties.setProperty("mail.smtp.port", "465");
		this.properties.setProperty("mail.smtp.auth", "true");
		this.properties.setProperty("mail.smtp.ssl.protocols", "TLSv1.2"); // this is required for authentication on some machines (specifically windows 11)
	}
	
	
	
	//=========//
	// methods //
	//=========//
	
	public void SendEmailLogErrors(String messageSubject, String messageContents)
	{
		try
		{
			EmailApi.INSTANCE.sendEmail(messageSubject, messageContents);
		}
		catch (Exception e)
		{
			System.out.println(WebReader.getCurrentDate() + " - email not sent. Error: [" + e.getMessage() + "]");
			e.printStackTrace();
		}
	}
	
	/** @throws Exception if there was an issue sending the email */
	public void sendEmail(String messageSubject, String messageContents) throws Exception
	{
		// Create a connection to the mail server		(good grief Java that is a lot of boilerplate.)
		Session session = Session.getDefaultInstance(this.properties, new Authenticator() { protected PasswordAuthentication getPasswordAuthentication() { return new PasswordAuthentication(senderUsername, senderPassword); } });
		
		MimeMessage message = new MimeMessage(session);
		message.setFrom(new InternetAddress(this.senderEmail));
		message.addRecipient(Message.RecipientType.TO, new InternetAddress(this.recipientEmail));
		
		message.setSubject(messageSubject);
		message.setText(messageContents);
		
		Transport.send(message);
		System.out.println(WebReader.getCurrentDate() + " - E-mail sent to [" + this.recipientEmail + "].");
	}
	
	
	
}
