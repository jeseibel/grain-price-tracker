package com.seibel.controllers;

import java.io.*;
import java.net.ConnectException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;

// TODO replace with CompletableFuture so exceptions can propagate correctly
// TODO add logging method to auto format everything
public class WebReader implements Runnable
{
	/** the website to get data from */
	public static final String WEBSITE_URL = "https://arthurcompanies.com/locations/harvey-pairie-towers/";
	public static final String GECKO_FILE_NAME = "geckodriver-v0.34.0-win32.exe";
	public static final int TIMEOUT_IN_SEC = 5; 
	
	
	/** This is how we interact with the website */
	private WebDriver driver;
	
	/** This is where we save files too */
	private final File exportDir;
	
	/** This is how we separate the data in our files */
	private final char delimiter;
	
	/** This is the extension each file will have when written */
	private final String fileExtension;
	
	
	
	//=============//
	// constructor //
	//=============//
	
	/** @param exportDirectory The directory to write data to */
	public WebReader(File exportDirectory, char newDelimiter, String newFileExtension) throws FileNotFoundException
	{
		this.exportDir = exportDirectory;
		if (!this.exportDir.exists())
		{
			if (!this.exportDir.mkdirs())
			{
				throw new FileNotFoundException("Unable to create the export directory.");
			}
			else
			{
				System.out.println("Created new export directory at: ["+this.exportDir.getPath()+"]");
			}
		}
		
		
		this.delimiter = newDelimiter;
		this.fileExtension = newFileExtension;
		
		// test to make sure that firefox is working correctly
		System.out.println("Opening Firefox...");
		this.openBrowser();
		System.out.println("Firefox successfully opened and connected...");
	}
	
	@Override
	public void run()
	{
		try
		{
			System.out.println(getCurrentDate() + " - reloading website...");
			
			// refresh the webpage
			this.refreshBrowser();
			
			System.out.println(getCurrentDate() + " - accessing website...");
			
			// get the data from the website
			HashMap<String, String> cashBidTableTextByCropName = this.getCashBidsTableText();
			if (cashBidTableTextByCropName.size() == 0)
			{
				throw new NotFoundException("Unable to find Cash Bids, site HTML may have changed.");
			}
			
			System.out.println(getCurrentDate() + " - writing data to file...");
			
			// write the data to a file
			this.parseCashBidTablesAndWriteToFile(cashBidTableTextByCropName);
		}
		catch (Exception e)
		{
			StringBuilder stackTraceBuilder = new StringBuilder();
			for (StackTraceElement element : e.getStackTrace())
			{
				stackTraceBuilder.append(element.toString()).append("\n");
			}
			
			
			// log the issue
			if (e.getClass().equals(ConnectException.class))
			{
				System.err.println(getCurrentDate() + " - ERROR: browser wasn't opened, or couldn't connect to: " + WEBSITE_URL);
			}
			else
			{
				System.err.println(getCurrentDate() + " - ERROR: Unexpected web reader issue, error: "+e.getMessage()+"\n"+stackTraceBuilder);
			}
			
			
			// send an email about the issue
			EmailApi.INSTANCE.SendEmailLogErrors(
				"Grain Price Tracker Error", 
				"Unable to read cash bids from: "+WEBSITE_URL+"\n"+
					"\n" +
					"Error: "+e.getMessage()+"\n" +
					"StackTrace: \n"+ stackTraceBuilder
				);
		}
	}

	
	
	//===================//
	// website interface //
	//===================//
	
	private HashMap<String, String> getCashBidsTableText() 
	{
		HashMap<String, String> tableContentsByCropName = new HashMap<>();
		
		// look through all header elements for the "cash bids" table header
		List<WebElement> tableElements = this.driver.findElements(By.name("cashbids-data-table"));
		for (WebElement element : tableElements)
		{
			WebElement parentElement = element.findElement(By.xpath("./../table/tbody/tr/th"));
			
			String cropName = parentElement.getText().trim();
			String tableText = element.getText().trim();
			tableContentsByCropName.put(cropName, tableText);
		}
		
		return tableContentsByCropName;
	}
	
	
	
	//===============//
	// data handling //
	//===============//
	
	/** This takes in the cash bid string and writes the relevant data to a file. */
	private void parseCashBidTablesAndWriteToFile(HashMap<String, String> cashBidTableTextByCropName)
	{
		HashSet<String> thresholdBreakingCropSet = new HashSet<>();
		String notificationEmailContent = "Threshold broken for the following crops: \n";
		
		
		for (String cropName : cashBidTableTextByCropName.keySet())
		{
			if (cropName.isEmpty())
			{
				System.out.println(getCurrentDate() + " - ERROR: empty crop name given for table, skipping...\n");
				continue;
			}
			
			
			String tableText = cashBidTableTextByCropName.get(cropName);
			BufferedReader reader = new BufferedReader(new StringReader(tableText));
			try
			{
				String line = reader.readLine();
				
				// keep going until we reach the end of the string
				while(line != null)
				{
					// ignore the header
					if(!line.toUpperCase().contains("DELIVERY"))
					{
						try
						{
							// parse this data line
							
							String[] parsedData = this.parseTableLine(line, reader);
							this.exportTableData(cropName, parsedData);
							
							
							// check if this crop's price is past the threshold
							String cashPrice = parsedData[2];
							if (this.priceAboveNotificationThreshold(cropName, cashPrice))
							{
								notificationEmailContent += "[" + cropName + "] price: [" + cashPrice + "] \t threshold: [" + this.getThresholdAmountForCrop(cropName) + "] \n";
								thresholdBreakingCropSet.add(cropName);
							}
							
							System.out.println(getCurrentDate() + " - parsed line: ["+line+"].");
						}
						catch (Exception e)
						{
							System.out.println(getCurrentDate() + " - ERROR: Failed to read line: ["+line+"].");
						}
					}
					
					line = reader.readLine();
				}
				
				System.out.println(getCurrentDate() + " - File writing completed.\n");
			}
			catch(IOException e)
			{
				// this shouldn't ever happen, since the reader is just
				// reading a local string
				System.out.println(getCurrentDate() + " - ERROR: string interpretation failed.\n");
			}
			
		}
		
		// send an email if 1 or more crops are above the price threshold
		if (thresholdBreakingCropSet.size() > 0)
		{
			String subject = "Crop Price Threshold Exceeded for [" + thresholdBreakingCropSet.size() + "] Crops";
			System.out.println("Crop threshold broken attempting to send email with subject: ["+subject+"]");
			EmailApi.INSTANCE.SendEmailLogErrors(subject, notificationEmailContent);
		}
	}
	
	/** This writes a given array of data to a file with the name cropName. */
	private void exportTableData(String cropName, String[] dataArray)
	{
		File file = new File(exportDir + "/" + cropName + fileExtension);
		
		// check if this file already exists
		boolean fileExists = file.isFile();
		
		try
		{
			// only create the file writer after we have checked if the file exists
			FileWriter fileWriter = new FileWriter(file,true);
			
			// add the file header if the file didn't exist yet
			if(!fileExists)
			{
				fileWriter.append("Contract Due Date" + delimiter +
						"Data Point Date" + delimiter +
						"Cash Price" + delimiter +
						//"Futures Month" + delimiter +
						"Futures Price" + delimiter +
						"Futures Change" + delimiter +
						"Basis\n");
			}
			
			// write all data to the file
			for (String dataElement : dataArray)
			{
				fileWriter.append(dataElement).append(delimiter);
			}
			fileWriter.append('\n');
			
			// save the file
			fileWriter.close();
		}
		catch(IOException e)
		{
			// the most likely cause of this, is the file is open in some other program
			System.out.println(getCurrentDate() + " - ERROR: " + file.getName() + " couldn't be written to.");
			System.out.println("Make sure that the file wasn't open with another program.\n");
		}
	}
	
	/** Returns true if the config registered threshold is less than the given cashPrice */
	private boolean priceAboveNotificationThreshold(String cropName, String cashPriceString)
	{
		double threshold = getThresholdAmountForCrop(cropName);
		
		if (threshold != -1)
		{
			try
			{
				double cashPrice = Double.parseDouble(cashPriceString);
				return cashPrice > threshold;
			}
			catch(NumberFormatException e)
			{
				System.out.println("Unable to convert Cash Price for crop [" + cropName + "] into a valid number. Cash Price: [" + cashPriceString + "]");
			}
		}
		
		return false;
	}
	
	/** 
	 * Returns the threshold for the given cropName 
	 * if one is present in the config. 
	 * If no threshold exists, returns -1.
	 */
	private double getThresholdAmountForCrop(String cropName)
	{
		ArrayList<String[]> thresholdByCropName = ConfigHandler.INSTANCE.getAllPairsLike(ConfigHandler.NOTIFY_THRESHOLD_KEY);
		
		for(String[] pair : thresholdByCropName)
		{
			String thresholdCropName = pair[0].substring(pair[0].indexOf('[') + 1, pair[0].indexOf(']'));
			
			if (thresholdCropName.equals(cropName))
			{
				try
				{
					return Double.parseDouble(pair[1]);
				}
				catch(NumberFormatException e)
				{
					System.out.println("Unable to convert Threshold for crop [" + cropName + "] into a valid number. Threshold: [" + pair[1] + "]");
					return -1;
				}
			}
		}
		
		return -1;
	}
	
	/**
	 * This takes in a string and bufferedReader, and outputs the
	 * information we want to save as an array.
	 * 
	 * @param initialLine The initial line from the cashBidString
	 * @param bf the bufferedReader, so we can continue reading in the cashBidString
	 * @return An array of Strings that contain the data we want to write to file
	 * 
	 * @throws IOException if the BufferedReader has issues reading in the String
	 */
	private String[] parseTableLine(String initialLine, BufferedReader bf) throws IOException
	{
		String str = initialLine;
		
		// Delivery 	Cash Price 	Futures Month 	Futures Price 	Futures Change 	Basis
		
		// contract Delivery Date
		int index = findEndOfDate(str);
		String delivery = str.substring(0, index);
		str = str.substring(index, str.length()).trim();
		
		// cash price
		index = findNextChar(str,' ',0);
		String cashPrice = str.substring(0, index);
		str = str.substring(index, str.length()).trim();
		
		// futures price
		String futuresPrice = convertFutureToDollarString(bf.readLine().trim());
		
		//futures change
		String futuresChange = convertFutureToDollarString(bf.readLine().trim());
		
		// basis
		String basis = bf.readLine().trim();
		
		
		String array[] = { delivery, getCurrentDate(), cashPrice, futuresPrice, futuresChange, basis };
		return array;
	}
	
	
	
	//================//
	// util functions //
	//================//
	
	/**
	 * This converts a future number into a regular dollar amount.
	 * IE: 670'4 = $6.705.
	 * Where the later number is: #'2 = #.0025 #'4 = #.005 #'6 = #.0075
	 * 
	 * @param str the string to convert
	 * 
	 * @return a string representation of the dollar amount.
	 */
	public static String convertFutureToDollarString(String str)
	{
		str = str.trim();
		
		// find where the comma is
		int commaIndex = findNextChar(str,'\'',0);
		
		// this string holds the number after the comma
		String partialCentString = str.substring(commaIndex+1,str.length());
		// make sure that there aren't any floating letters after the number
		partialCentString = partialCentString.replaceAll("[A-Za-z]", "");
		
		
		// determine the cent portion
		int futurePartialCent = 0;
		if (commaIndex != -1)
		{
			try
			{
				int futurePartialCentInt = Integer.parseInt(partialCentString);
				
				// determine if the number after the comma is a 0, 2, 4, or 6
				switch (futurePartialCentInt)
				{
					case 0:
						futurePartialCent = 0;
						break;
					case 2:
						futurePartialCent = 25;
						break;
					case 4:
						futurePartialCent = 5;
						break;
					case 6:
						futurePartialCent = 75;
						break;
				}
				
			}
			catch (NumberFormatException e)
			{
				System.out.println(getCurrentDate() + " - Warning: [" + str + "] failed partial cent conversion, defaulting to [0].");
			}
		}
		
		String futureCentString = (commaIndex != -1) ? str.substring(0,commaIndex) : "";
		String dollarString = "";
		
		// determine the dollar amount
		try
		{
			// if there is no first number, then it must be zero
			if(!futureCentString.isEmpty())
			{
				if(!futureCentString.equals("-"))
				{
					dollarString = Integer.parseInt(futureCentString) + "";
				}
				else
				{
					dollarString = "-0";
				}
			}
			else
			{
				dollarString = "0";
			}
			
		}
		catch(NumberFormatException e)
		{
			System.out.println(getCurrentDate() + " - ERROR: \"" + str + "\" failed future full cent conversion.");
		}
		
		// this is an example of how different length Strings
		// need to be cut differently
		// 4	->  0.04
		// 45	->  0.45
		// 456	->  4.56
		// 4567	-> 45.67
		
		String sign = "";
		if(dollarString.subSequence(0,1).equals("-"))
		{
			sign = "-";
			dollarString = dollarString.substring(1,dollarString.length());
		}
		
		// split the dollarString so that the decimal place
		// falls correctly (see example above)
		if(dollarString.length() > 2)
		{
			dollarString = sign + dollarString.substring(0,dollarString.length() - 2) 
					+ "." 
					+ dollarString.substring(dollarString.length()-2, dollarString.length());
		}
		else if(dollarString.length() == 2)
		{
			dollarString = sign + "0." + 
					dollarString.substring(dollarString.length()-2, dollarString.length());
		}
		else if(dollarString.length() == 1)
		{
			dollarString = sign + "0" + 
					".0" + dollarString.substring(0,dollarString.length());
		}
		else
		{
			// this shouldn't happen, but just in case
			dollarString = sign + "0.";
		}
		
		// combine the two pieces
		return dollarString + futurePartialCent;
	}
	
	/**
	 * @return a formatted string containing the current time and date;
	 * 		   in the format "MM/dd/yyyy hh:mm am/pm"
	 */
	public static String getCurrentDate() { return new SimpleDateFormat("MM/dd/yyyy hh:mm a").format(new Date(System.currentTimeMillis())); }
	
	/**
	 * This is a helper function to determine the ending index of the date
	 * in the cashBidString.
	 * 
	 * @return the index for the end of the date
	 */
	private static int findEndOfDate(String cashBidString)
	{
		int i = 0;
		
		if (cashBidString.startsWith("Harvest"))
		{
			// date format: "Harvest '22"
			i = findNextChar(cashBidString, ' ',0);
			i = findNextChar(cashBidString, ' ',i+1); // find the 2nd space
		}
		else 
		{
			// date format: "Mar 31, 2020"
			i = findNextChar(cashBidString, ' ',0);
			i = findNextChar(cashBidString, ' ',i+1);
			i = findNextChar(cashBidString, ' ',i+1); // find the 3rd space
		}
		
		return i;
	}
	
	/**
	 * This method takes in a string and returns the index of 
	 * the first character that matches charToFind
	 * after the startingIndex.
	 * 
	 * @param str the string to look through
	 * @param startingIndex where to start looking for charToFind
	 * 
	 * @return the index of the first charToFind in s
	 */
	public static int findNextChar(String str, char charToFind, int startingIndex)
	{
		str = str.substring(startingIndex);
		return str.indexOf(charToFind) + startingIndex;
	}
	
	/**
	 * This takes in a cropName and determines which month is important for it.
	 * NOTE: this can return null, if the crop doesn't match anything.
	 * 
	 * @return A shorthand version of the month (IE: December -> Dec, November -> Nov)
	 */
	public static String determineSigMonth(String cropName)
	{
		if(cropName.contains("NS"))
		{
			// Wheat cares about September
			return "Sep";
		}
		else if(cropName.contains("SOY"))
		{
			// Soybeans care about November
			return "Nov";
		}
		else if(cropName.contains("CORN"))
		{
			// Corn cares about December
			return "Dec";
		}
		else
		{
			// This crop doesn't have a known significant month
			return null;
		}
	}
	
	
	
	//=================//
	// browser control //
	//=================//
	
	/** //@throws ConnectException if the browser exe is missing, requires updates, or can't connect to the website */
	private void openBrowser() throws FileNotFoundException
	{
		// Launch Firefox
		try
		{
			// TODO embed the exe file in the jar instead of requiring it to be next to the jar
			File geckoDriverFile = new File(GECKO_FILE_NAME); // .exe file next to the jar
			
			// check if the firefox driver exe is present
			System.out.println("Looking for ["+geckoDriverFile.getName()+"] at the path: ["+geckoDriverFile.getAbsolutePath()+"]");
			if (!geckoDriverFile.exists())
			{
				throw new FileNotFoundException("geckodriver.exe wasn't found next to the jar file, unable to start the firefox driver.");
			}
			
			// tell Selenium where the firefox driver is
			System.setProperty("webdriver.gecko.driver",GECKO_FILE_NAME);
			
			// print the log to a text file instead of to the console
			System.setProperty(FirefoxDriver.SystemProperty.DRIVER_USE_MARIONETTE,"true");
			System.setProperty(FirefoxDriver.SystemProperty.BROWSER_LOGFILE,"log.txt");
			
			// launch Firefox
			this.driver = new FirefoxDriver();
			this.driver.manage().timeouts().implicitlyWait(TIMEOUT_IN_SEC, TimeUnit.SECONDS);
			//hideBrowser();
			
			// go to the specified website
			this.driver.get(WEBSITE_URL);
			
			// print out when the page loads
			System.out.println(getCurrentDate() + " - Webpage loaded: " + this.driver.getTitle());
		}
		catch(Exception e)
		{
			// make sure the driver is shut down, otherwise gecko will keep running in the background
			if (this.driver != null)
			{
				this.driver.quit();
			}
			
			System.out.println(getCurrentDate() + " - ERROR: "+e.getMessage());
			e.printStackTrace();
			throw e;
		}
	}
	
	/**
	 * This causes the browser to reload the page at WEBSITE_URL
	 * @throws ConnectException if the connection fails
	 */
	private void refreshBrowser() throws ConnectException
	{
		try
		{
			// go to the specified website
			driver.get(WEBSITE_URL);
			
			// print out when the page loads
			System.out.println(getCurrentDate() + " - Webpage refreshed: " + driver.getTitle());
		}
		catch(Exception e)
		{
			System.out.println(getCurrentDate() + " - ERROR: webpage couldn't be loaded");
			e.printStackTrace();
			
			// quit the driver, the connection failed
			driver.quit();
			
			throw new ConnectException();
		}
	}
	
	/**
	 * This cleans up any loose ends
	 * and closes all external resources.
	 * NOTE: calling on this object after this has been called will cause errors!
	 */
	public void quit() { closeBrowser(); }
	
	private void closeBrowser() { if(driver != null) driver.quit(); }
	
	/** Moves the browser offscreen. */
	public void hideBrowser() { driver.manage().window().setPosition(new Point(-2000, 0)); }
	/** Bring the browser on screen. */
	public void showBrowser() { driver.manage().window().setPosition(new Point(0, 0)); }
	
}
