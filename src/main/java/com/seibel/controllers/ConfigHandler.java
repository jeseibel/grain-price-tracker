package com.seibel.controllers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Pattern;

/**
 * 
 * @author James_Seibel
 * @version 2-12-2022
 */
public class ConfigHandler
{
	public static final String EXPORT_FILE_LOCATION_KEY = "ExportFileLocation";
	public static final String NOTIFY_THRESHOLD_KEY = "CashNotifyThreshold";
	public static final String SENDER_EMAIL_PASSWORD_KEY = "SenderEmailPassword";
	public static final String SENDER_EMAIL_USERNAME_KEY = "SenderEmailUsername";
	public static final String SENDER_EMAIL_KEY = "SenderEmail";
	public static final String RECIPIENT_EMAIL_KEY = "RecipientEmail";
	
	public static final String[] OBFUSCATED_VALUE_KEYS = new String[]{ SENDER_EMAIL_PASSWORD_KEY };
	
	// needs to be after the other static values to make sure they are all loaded 
	public static final ConfigHandler INSTANCE = new ConfigHandler();
	
	
	/** This is the config file and holds information needed between program runs */
	private final File CONFIG_FILE = new File("config.cfg");
	
	private final HashMap<String, String> CONFIG_VALUE_BY_KEY = new HashMap<>();
	
	
	
	private ConfigHandler()
	{
		try
		{
			if(CONFIG_FILE.exists())
			{
				BufferedReader br = new BufferedReader(new FileReader(CONFIG_FILE));
				
				System.out.println("Config file found, reading...");
				
				Pattern regex = Pattern.compile("\\\" : \\\"");
				String line = br.readLine();
				while (line != null)
				{
					String[] lineSplit = regex.split(line);
					
					if (lineSplit.length < 2)
					{
						//System.out.println("line [" + line + "] didn't have a separator, ignoring.");
						line = br.readLine();
						continue;
					}
					else if (lineSplit.length > 2)
					{
						System.out.println("line [" + line + "] had too many separators, ignoring.");
						line = br.readLine();
						continue;
					}
					
					String key = lineSplit[0];
					key = key.substring(key.indexOf('"') + 1);
					String value = lineSplit[1];
					value = value.substring(0, value.lastIndexOf('"') != -1 ? value.lastIndexOf('"') : value.length()); // TODO make this cleaner
					
					this.CONFIG_VALUE_BY_KEY.put(key, value);
					
					String displayValue = value;
					if (shouldValueBeObfuscated(key))
					{
						displayValue = "*".repeat(6);
					}
					System.out.println("Config entry [" + key + "] : [" + displayValue + "] found and added.");
					
					line = br.readLine();
				}
				
				System.out.println("Config file reading finished.");
				
				br.close();
			}
			else
			{
				System.out.println("Config file not found.");
			}
		}
		catch (FileNotFoundException e)
		{
			System.out.println("NOTE: the config file doesn't exist, a new one will be created...");
		}
		catch(IOException e)
		{
			System.out.println("ERROR: failed to read the config file.");
		}
		
	}
	private boolean shouldValueBeObfuscated(String key)
	{
		for (String obfuscatedKey : OBFUSCATED_VALUE_KEYS)
		{
			if (key.equalsIgnoreCase(obfuscatedKey))
			{
				return true;
			}
		}
		
		return false;
	}
	
	
	
	/** @return null if no entry exists for the given key */
	public String getValue(String key)
	{
		return this.CONFIG_VALUE_BY_KEY.get(key);
	}
	
	/** @return every config value whose key contains the searchKey */
	public ArrayList<String[]> getAllPairsLike(String searchKey)
	{
		ArrayList<String[]> values = new ArrayList<>();
		for (String key : CONFIG_VALUE_BY_KEY.keySet())
		{
			if (CONFIG_VALUE_BY_KEY.containsKey(key) && key.contains(searchKey))
			{
				values.add(new String[] {key, CONFIG_VALUE_BY_KEY.get(key)});
			}
		}
		return values;
	}
	
	/** @throws IllegalArgumentException if the value has a quote in it */
	public void setValue(String key, String value) throws IllegalArgumentException
	{
		if (value.contains("\" : \""))
		{
			throw new IllegalArgumentException("Error: Unable to add the config value [" + value + "] for the key [" + key + "]. Config values can not contain a colon surrounded by quotes IE: [value\" : \"value].");
		}
		
		CONFIG_VALUE_BY_KEY.put(key, value);
		
		// update the file
		saveToFile();
	}
	
	
	private void saveToFile()
	{
		boolean success = true;
		
		try
		{
			// delete and re-create the file to clear it
			if (CONFIG_FILE.exists())
			{
				CONFIG_FILE.delete();
			}
			CONFIG_FILE.createNewFile();
			
			
			FileWriter fw = new FileWriter(CONFIG_FILE);
			for (String key : CONFIG_VALUE_BY_KEY.keySet())
			{
				if (CONFIG_VALUE_BY_KEY.containsKey(key))
				{
					String value = CONFIG_VALUE_BY_KEY.get(key);
					String lineToWrite = "\"" + key + "\" : \"" + value + "\"\n";
					fw.write(lineToWrite);
				}
			}
			
			fw.close();
		}
		catch (IOException e)
		{
			success = false;
			System.out.println("ERROR: config file writing failed. Error: [" + e.getMessage() + "].");
			e.printStackTrace();
		}
		
		System.out.println(success ? "Config file successfully saved." : "Config file saving failed!");
	}
	
}
